import React, { Component } from 'react'

class RobotForm extends Component {

    constructor(props){
        super(props);
        this.state = {
            id: 0,
            type: '',
            name: '',
            mass: 0
        };

        this.handleInputChange = (e) => {
            this.setState({
                [e.target.name]: e.target.value
            })
        }
    }

    render() {
        return (
            <form>
                <label>Name</label>
                <input id="name" name="name" onChange={this.handleInputChange} />
                <label>Type</label>
                <input id="type" name="type" onChange={this.handleInputChange} />
                <label>Mass</label>
                <input id="mass" name="mass" onChange={this.handleInputChange} />
                <input type="button" value="add" onClick={this.handleAdd} /> 
            </form>
        );
    }

    handleAdd = () => {
        let item = {...this.state};
        this.props.onAdd(item);
    }
}

export default RobotForm
