import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'
import Robot from './Robot'
import RobotForm from './RobotForm';



class RobotList extends Component {
	constructor(){
		super()
		this.state = {
			robots : []
		}
	}
	componentDidMount(){
		this.store = new RobotStore()
		this.setState({
			robots : this.store.getRobots()
		})
		this.store.emitter.addListener('UPDATE', () => {
			this.setState({
				robots : this.store.getRobots()
			})			
		})
	}

	handleAdd = (evt) => {
		this.store.addRobot(evt);
		console.log('store', this.store.robots);
		console.log('state', this.state.robots);
	}

	render() {
		return (
			<div>				 
				{
					this.state.robots.map((e, i) => 
						<Robot item={e} key={i} />
					)
				}
				<RobotForm onAdd={this.handleAdd} />
			</div>
		)
	}
}

export default RobotList
